import React, { Component } from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import {Provider} from 'react-redux';

// import redux
import Store from './Store/index';

// Import Routes
import Routes from './Routes/Routes';

class App extends Component {
  render() {
    return (
      <section>
        <Provider store={Store}> 
            <Router>
              <Switch>
                <Route component={Routes} />
              </Switch>
            </Router>
        </Provider>
      </section>
    )
  }
}

export default App;