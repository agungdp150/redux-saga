import React, { Component } from 'react'
import {Link} from "react-router-dom";


// Import Layout
import CardHome from '../Components/CardHome/CardHome';


class Homepage extends Component {
    render() {
        return (
            <div>
                <h1>Hello From Homepage</h1>
                <CardHome />
                <Link to="/signin">
                <button>
                    go to login
                </button>
                </Link>
            </div>
        )
    }
}


export default Homepage;
