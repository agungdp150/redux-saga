import React, { Component } from 'react'

class PageAfterLogin extends Component {

    removeToken = () => {
        localStorage.clear();
        this.props.history.push('/signin')
    }

    render() {
        return (
            <div>
                <h1>After Login</h1>
                <button onClick={this.removeToken}>Sign Out</button>
            </div>
        )
    }
}


export default PageAfterLogin;