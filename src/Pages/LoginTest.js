import React, { Component } from 'react'
import {connect} from 'react-redux';
import { loginAction } from '../Store/Actions/LoginAction';




class LoginTest extends Component {

    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            checkedToken: ''
        }
    }

    handleChangeField = e => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleLogin = async e => {
        e.preventDefault();

        const sendaDataLogin = {
            email: this.state.email,
            password: this.state.password
        };
        await this.props.loginAction(sendaDataLogin);
        this.setState({
            checkedToken: localStorage.getItem('token')
        })
    }


    render() {

        const {email, password, checkedToken} = this.state;

        return (
            <div>

                {
                    checkedToken ? 
                    (
                        <div>
                        <h3>Yeay Login Bro</h3>
                        </div>
                    ) :
                    (
                        <div>
                        <h3>Login page</h3>
                        <form onSubmit={this.handleLogin}>
                            <input 
                            name="email"
                            type="email"
                            value={email}
                            placeholder="fill your email"
                            onChange={this.handleChangeField}
                            />
        
                            <input 
                            name="password"
                            type="password"
                            value={password}
                            placeholder="set password"
                            onChange={this.handleChangeField}
                            />
        
                            <button type="submit">
                                Login
                            </button>
                        </form>
                        </div>
                   )
                }

            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        loginRes: state.LoginData.loginRes
    }
}

export default connect(
    mapStateToProps,
    {loginAction}
) (LoginTest);
