import React, { Component } from 'react';
import axios from "axios";

class PageLogin extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: ''
        }
    }

    handleChangeField = e => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleLogin = async e => {
        e.preventDefault();

        const sendaDataLogin = {
            email: this.state.email,
            password: this.state.password
        };

        const response = await axios.post('https://reqres.in/api/login', sendaDataLogin);
        console.log(response, "login success");

        localStorage.setItem('token', response.data.token);

        this.props.history.push("/afterlogin");
    }



    render() {

        const {email, password} = this.state;

        return (
            <div>
                <h3>Login page</h3>
                <form onSubmit={this.handleLogin}>
                    <input 
                    name="email"
                    type="email"
                    value={email}
                    placeholder="fill your email"
                    onChange={this.handleChangeField}
                    />

                    <input 
                    name="password"
                    type="password"
                    value={password}
                    placeholder="set password"
                    onChange={this.handleChangeField}
                    />

                    <button type="submit">
                        Login
                    </button>
                </form>
            </div>
        )
    }
}


export default PageLogin;