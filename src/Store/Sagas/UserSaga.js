import {call, put, takeEvery} from 'redux-saga/effects';
import axios from "axios";

const apiUrl="http://jsonplaceholder.typicode.com/users"

async function getApiUsers() {
    const response = await axios.get(apiUrl);
    console.log(response.data, 'from saga');
    return response.data
}

function* fetchUsers(action) {
    try {
        const users = yield call(getApiUsers)
        yield put({type: 'GET_USERS_SUCCESS', users: users})
    } catch(err) {
        console.log(err)
        yield put({type: 'GET_USERS_FAIL', message: err.message})
    }
}

function* userSaga() {
    yield takeEvery('GET_USERS_REQUESTED', fetchUsers);
}

export default userSaga;