import {call, put, takeEvery} from 'redux-saga/effects';
import * as type from '../types';
import axios from 'axios';


const apiLogin = 'https://reqres.in/api/login'

function* loginSend(action) {
    try {
        console.log(action.payload)
        const response = axios.post(apiLogin, action.payload);
        console.log(response.data);
        const login = yield call(response.data)
        yield put({type: type.LOGIN_USER_SUCCESS, login: login})
    } catch(err) {
        console.log(err)
        console.log(err.message)
        yield put({type: type.LOGIN_USER_FAIL, message: err.message})
    }
}

function* loginSaga() {
    yield takeEvery(type.LOGIN_USER_REQUESTED, loginSend);
}

export default loginSaga;