import {all} from 'redux-saga/effects';
import userSaga from './UserSaga';
import loginSaga from './LoginSaga';

export default function* rootSaga() {
    yield all([
        userSaga(),
        loginSaga()
    ])
}