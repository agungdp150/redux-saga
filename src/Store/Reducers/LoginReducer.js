import * as types from '../types';

const initialState = {
    token: localStorage.getItem('token'),
    isAuthenticated: null,
    loading: true,
    user: null
}


function loginUser(state = initialState, action) {
    switch (action.type) {
        case types.LOGIN_USER_REQUESTED:
            return {
                ...state,
                loading: true
            }
        case types.LOGIN_USER_SUCCESS:
            localStorage.setItem('token', action.payload.result.token);
            return {
                ...state,
                loading: false,
                user: action.token
            }
        case types.LOGIN_USER_FAIL:
            return {
                ...state,
                loading: false,
            }
        default:
            return state;
    }
}

export default loginUser;
  