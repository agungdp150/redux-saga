import {combineReducers} from 'redux';

// import reducer
import UsersReducer from './UserReducer';
import LoginReducer from './LoginReducer'

const rootReducer = combineReducers({
    UsersList: UsersReducer,
    LoginData: LoginReducer
});

export default rootReducer;