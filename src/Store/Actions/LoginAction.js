import * as type from '../types';


export function loginAction(loginRes) {
    return {
        type: type.LOGIN_USER_REQUESTED,
        payload: loginRes
    }
}

