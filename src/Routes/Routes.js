import React from "react";
import { Route, Switch } from "react-router-dom";

// Layout
import Navbar from "../Components/Navbar/Navbar";
import Footer from "../Components/Footer/Footer";

// Import Page
import Homepage from "../Pages/Homepage";
import PageLogin from "../Pages/PageLogin";
import PageAfterLogin from "../Pages/PageAfterLogin";
import LoginTest from "../Pages/LoginTest";



const routes = () => {

    const AppRoute = ({component : Component, layout : Layout, ...rest}) => (
        <Route {...rest} render={props => (
          <Layout>
            <Component {...props} />
          </Layout>
        )} />
    )
    
    const NavFoot = props => (
        <div>
          <Navbar/>
            {props.children}
          <Footer/>
        </div>
      )
    
      const DefaultLayout = props => (
        <div>
          {props.children}
        </div>
      )


    return (
        <section>
            <Switch>
                <AppRoute exact path="/" layout={NavFoot} component={Homepage} />
                <AppRoute exact path="/signin" layout={NavFoot} component={PageLogin} />
                <AppRoute exact path="/afterlogin" layout={NavFoot} component={PageAfterLogin} />
                <AppRoute exact path="/testlogin" layout={NavFoot} component={LoginTest} />
            </Switch>
        </section>
    )
}


export default routes;