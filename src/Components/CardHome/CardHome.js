import React, { Component } from 'react'
import { connect } from 'react-redux';
import { getUsers } from '../../Store/Actions/UserAction';

class CardHome extends Component {

    componentDidMount() {
        this.props.getUsers();
    }

    render() {
        const check = this.props.users.map((data, i) => {
            return (
                <div key={i}>
                    <p>{data.name}</p>
                </div>
            )
        })
        return (
            <div>
                {check}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        users: state.UsersList.users
    }
}

export default connect(
    mapStateToProps,
    {getUsers}
) (CardHome);